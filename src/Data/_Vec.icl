implementation module Data._Vec

import StdClass
import StdInt
import StdMisc
import StdOverloaded
import _SystemArray

DEFAULT_RESERVE :== 16
DEFAULT_CAPACITY :== 16

newVecWithCapacity :: !Int -> *Vec a
newVecWithCapacity size =
	{ size = 0
	, reserve = DEFAULT_RESERVE
	, data = createArray size empty_vector_element
	}

newVecWithReserveAndCapacity :: !Int !Int -> *Vec a
newVecWithReserveAndCapacity reserve size =
	{ size = 0
	, reserve = reserve
	, data = createArray size empty_vector_element
	}

newVec :: *Vec a
newVec =
	{ size = 0
	, reserve = DEFAULT_RESERVE
	, data = createArray 0 empty_vector_element
	}

empty_vector_element :: .a
empty_vector_element = abort "evaluated empty Vec element\n"

instance Array Vec a
where
	select {size,data} i
		| i < 0
			= abort "Data.Vec.select: index < 0\n"
		| i >= size
			= abort "Data.Vec.select: index >= size\n"
		| otherwise
			= data.[i]

	uselect vec=:{size} i
		| i < 0
			= abort "Data.Vec.uselect: index < 0\n"
		| i >= size
			= abort "Data.Vec.uselect: index >= size\n"
		| otherwise
			= vec!data.[i]

	size {size} = size

	usize vec=:{size} = (size, vec)

	update vec=:{size} i x
		| i < 0
			= abort "Data.Vec.update: index < 0\n"
		| i >= size
			= abort "Data.Vec.update: index >= size\n"
		| otherwise
			= {vec & data.[i]=x}

	createArray n x =
		{ size = n
		, reserve = 0
		, data = createArray n x
		}

	_createArray n =
		{ size = n
		, reserve = 0
		, data = _createArray n
		}

	replace vec=:{size,data} i x
		| i < 0
			= abort "Data.Vec.replace: index < 0\n"
		| i >= size
			= abort "Data.Vec.replace: index >= size\n"
			# (x, data) = replace data i x
			  vec & data = data
			= (x, vec)

instance +++ (Vec a)
where
	(+++) infixr 5 :: !(Vec a) !(Vec a) -> Vec a
	(+++) v1 v2
		# expectedSpace = max (size v1.data - v1.size - v2.size) 0
		// Since we are not dealing with unique Vecs, there is no need to actually fill up v1, we should, however, have
		// the behavior be consistent. For that reason, we allocate the expected size left after +++ instead of the
		// needed size.
		=
			{ v1
			& size = v1.size + v2.size
			, data = 
				{
					{ createArray (v1.size + v2.size + expectedSpace) empty_vector_element
					& [i] = v1.data.[i]
					\\ i <- [0 .. v1.size - 1]
					}
					& [i] = v2.data.[i - v1.size]
					\\ i <- [v1.size .. v1.size + v2.size - 1]
				}
			}

push :: a !*(Vec a) -> *Vec a
push x vec=:{size, reserve, data}
	# (sz, data) = usize data
	| size < sz	=
		{vec & size = size + 1, data.[size]=x}
	| reserve == 0 =
		abort "Data.Vec.push: vector full and reserve is 0\n"
	| otherwise =
		{ vec
		& size = size + 1
		, data =
			{ { createArray (size+reserve) empty_vector_element & [size] = x }
			& [i] = data.[i]
			\\ i <- [0 .. size - 1]
			}
		}

pop :: !*(Vec a) -> *(?a, !*Vec a)
pop vec=:{size, data}
	| size <= 0
		= (?None, vec)
	| otherwise
		# (x, data) = data![size - 1]
		# vec & data = data
		= (?Just x, {vec & size = size - 1})

setCapacity :: !Int !*(Vec a) -> *Vec a
setCapacity cap vec=:{size, data}
	# newCap = max cap size
	=
		{ vec
		& data =
			{ createArray newCap empty_vector_element
			& [i] = data.[i]
			\\ i <- [0 .. size - 1]
			}
		}

reserve :: !Int !*(Vec a) -> *Vec a
reserve res vec=:{size, data}
	# (sz, data) = usize data
	# newCap = sz + res
	=
		{ vec
		& data =
			{ createArray newCap empty_vector_element
			& [i] = data.[i]
			\\ i <- [0 .. size - 1]
			}
		}
