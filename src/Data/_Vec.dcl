definition module Data._Vec

from StdOverloaded import class +++
from _SystemArray import class Array

:: Vec a =
	{ size :: !Int
	/// Current size of the vector (the number of elements)
	, reserve :: !Int
	/// The amount of additional data the Vec should reserve if it runs out of available space
	, data :: !.{a}
	/// The elements stored in the Vec
	}

/**
 * Creates a new Vec with a given initial capacity and a specified reserve parameter
 * @param The initial reserve parameter
 * @param The initial capacity
 * @result A Vector	with the specified reserver and capacity
 */
newVecWithReserveAndCapacity :: !Int !Int -> *Vec a

/**
 * Creates a new Vec with a given initial capacity and a default reserve parameter
 */
newVecWithCapacity :: !Int -> *Vec a

/**
 * Creates a new Vec with a default initial capacity and a default reserve parameter
 */
newVec :: *Vec a

instance Array Vec a

/**
 * The reserve property of the first Vec is maintained if the second Vec fits into the space of the first Vec, it is
 * placed there. Otherwise a new Vec is created with capacity = (size v1 + size v2).
 */
instance +++ (Vec a)

/**
 * Push a new element to the end of the Vec. If the element no longer fits in the vector the vector's capacity is
 * increased by vec.reserve.
 * @param The element to push
 * @param The Vec to which the element should be pushed
 * @result The Vec with vec.[size - 1] = a
 */
push :: a !*(Vec a) -> *Vec a

/**
 * Pop an element from the end of the Vec.
 * @param The Vec from which to pop an element.
 * @result The element.
 * @result The Vector which a size one smaller and a different element at the end.
 */
pop :: !*(Vec a) -> *(?a, !*Vec a)

/**
 * Sets the capacity of the Vec to the specified amount. If the provided capacity is below the current size, the
 * capacity is set to the current size.
 * @param The desired capacity
 * @param The Vec
 * @result A Vec with the capacity set to the desired value
 */
setCapacity :: !Int !*(Vec a) -> *Vec a

/**
 * Reserve additional capacity in the Vec
 * @param The desired additional capacity
 * @param The Vec that has to be resized
 * @result A Vec with the desired increased capacity
 */
reserve :: !Int !*(Vec a) -> *Vec a
